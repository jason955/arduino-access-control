
// library to store the keys in EEPROM
// written by Jason Hamilton


// The microcontrollers on the various Arduino boards have different 
// amounts of EEPROM: 1024 bytes on the ATmega328, 512 bytes on the 
// ATmega168 and ATmega8, 4 KB (4096 bytes) on the ATmega1280 and ATmega2560.
// arduino.cc/en/Reference/EEPROM
# define EEPROM_SIZE (1024)


//////////////////
//  HEADER////////
//////////////////
class KeyMemory{
  public:
    KeyMemory(void);
    void reset();
    bool addKey(uint8_t key[]);
    bool isKeyPresent(uint8_t key[]);
    void dumpEeprom();
  private:
    int _keyCount;
    void _incrementKeyCount();
};



//////////////////////
/// IMPLEMENTATION ///
//////////////////////

KeyMemory::KeyMemory(){
  int keyCount = EEPROM.read(0);
  if (keyCount == 255){
    reset();
  }
  _keyCount = keyCount;
}

void KeyMemory::reset(){
  EEPROM.write(0,0);
  _keyCount = 0;
}

//add key to last spot in EEPROM
bool KeyMemory::addKey(uint8_t key[]){
  
  if (_keyCount > EEPROM_SIZE / 4 - 2 ){
    return 0;
  }
  
  else{
    _incrementKeyCount();
    int pos_4 = _keyCount * 4;
    int pos_3 = pos_4 - 1;
    int pos_2 = pos_3 - 1;
    int pos_1 = pos_2 - 1;
    
    EEPROM.write(pos_1, key[0]);
    EEPROM.write(pos_2, key[1]);
    EEPROM.write(pos_3, key[2]);
    EEPROM.write(pos_4, key[3]);
    
    return 1;
  }
  
}


bool KeyMemory::isKeyPresent(uint8_t key[]){
  int pos_1 = 0;
  int pos_2 = 0;
  int pos_3 = 0;
  int pos_4 = 0;
  if (_keyCount>0){
    for(int i=1; i<=_keyCount; i++){
      pos_1 = EEPROM.read(i*4 - 3);
      pos_2 = EEPROM.read(i*4 - 2);
      pos_3 = EEPROM.read(i*4 - 1);
      pos_4 = EEPROM.read(i*4);
      
      if (pos_1==key[0] && pos_2==key[1] && pos_3==key[2] && pos_4==key[3]){
        Serial.print("key found");
        return 1;
      }
    }
  }
  
  return 0;
}


void KeyMemory::dumpEeprom(){
  int value;
  for(int i = 0; i <= 255; i++){
    Serial.println(EEPROM.read(i));
  }


}

void KeyMemory::_incrementKeyCount(){
  _keyCount++;
  EEPROM.write(0,_keyCount);
}

