Access Control For Arduino
===========================

Overview
---------------------------

This is my Arduino-based project that allows you to use RFID for access control to open a door.  The door can be anything that can be controlled by a relay.  In my case it will be a garage door opener.

This is the initial prototype.  Next I plan to build it on a prototype shield and then if put it on a PCB.  The top section of components (Arduino and breadboard) will be placed inside the garage and the bottom section of components (LED, buzzer, NFC/RFID reader) will be placed outside (in a project box).

There is a master RFID key that is hard-coded into the software, which allows you to add other keys.  Basically, you scan the master, it will give you an audio and visual indicator and allow you to scan a key to add.  Once you scan a new key it is memorized in the EEPROM.  You can add as many keys as your EEPROM will hold, which in the case of my chip is 256 (the keys are 4 bytes).  You can clear all keys from the EEPROM (except the master) by holding the reset button.

![Alt text](https://bytebucket.org/jason955/arduino-access-control/raw/7ccab8d57553ba7634525d1005eaadc6a3ccce1c/Images/1.jpg)


Component Description
---------------------------

Top breadboard + components - from right to left:

- **Access relay** - Relay to activate door.
- **Reset button** - Hold for ~2000ms to clear all keys.
- **Button to activate access relay** - Hold for ~200ms to activate
- **4 pin switch** - Bit registers (I think that's the name?) for relay activation time (multiplied by 100ms).  Adjustable from 100ms to 1500ms in increments of 100ms.  Ex: 1+3 like in photo give 500ms of relay time.
- **Arduino Duemilanove** - ATMEGA 328 microcontroller project board.


Looking at the bottom half of components - from right to left:

- **RFID/NFC reader** - Reads NFC/RFID cards.
- **Audio and visual feedback module** - Red/green 2-color-led and a piezo buzzer.  This gives the user feedback when interacting with the device.
